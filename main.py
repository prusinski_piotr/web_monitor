from flask import Flask
from flask import render_template
import json
import requests
app = Flask(__name__)


def is_reachable(url):
    try:
        r = requests.get(url)
        return True
    except requests.ConnectionError:
        return False
    else:
        return False

def get_status_code(url):
    try:
        status_code = requests.get(url).status_code
        return status_code
    except requests.ConnectionError:
        return "NO CONNECTION"

def get_requirement_bool(url, requirement):
    r = requests.get(url)
    rtext = r.text

    if requirement in rtext:
        return True
    else:
        return False

def get_request_time(url):
    r = requests.get(url)
    rtime = r.elapsed.total_seconds()
    return rtime

@app.route('/')
def main():
    with open('config.json', 'r') as f:
        urls = json.load(f)
    for url in urls:
        if is_reachable(url['url']):
            url['status_code'] = get_status_code(url['url'])
            url['requirement_met'] = get_requirement_bool(url['url'], url['requirement'])
            url['request_time'] = get_request_time(url['url'])
        else:
            url['status_code'] = "NOT REACHABLE"
    
    
    return render_template('main.html', urls=urls)